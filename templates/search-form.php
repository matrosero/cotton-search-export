<?php

defined( 'ABSPATH' ) or exit; // or die;

$meta = cotton_get_je_meta_options();

$countries_options = cotton_countries_shortlist();//$meta['countries'];
$categories_options = $meta['categories'];
$radio_options = $meta['options'];

?>

  	
	<header class="company-search-heading">
		<h3>Search</h4>
	</header>
	<form method="get" id="cotton-search" action="<?php the_permalink() ?>">

		<fieldset class="general-fields">
			
			<?php

			if ( is_array($countries_options) && !empty($countries_options) ) { ?>
				<p>
					<label for="country">Country</label>
					<span class="field-instructions">Please select one or multiple countries:</span>

					<select name="country[]" class="js-select" id="country[]" multiple="multiple">

						<option value="">Choose one</option>

						<?php
						
						foreach ($countries_options as $option) { ?>
						
							<option value="<?php echo $option; ?>" <?php echo ( isset($_REQUEST['country']) && sanitize_text_field($_REQUEST['country']) && sanitize_text_field($_REQUEST['country']) == $option) ? 'selected="selected" ' : ''; ?>><?php echo $option; ?></option>

						<?php } ?>

					</select>
					<!-- <input type="text" name="country" id="country" value="<?php echo (isset($country)) ? $country : ''; ?>" /> -->
				</p>

			<?php }


			if ( is_array($categories_options) && !empty($categories_options) ) { ?>

				<p>
					<label for="category">Category</label>
					<span class="field-instructions">Please select one of the following options:</span>

					<select name="category" id="category">
					
						<option value="">Choose one</option>

						<?php
						foreach ($categories_options as $option) { ?>
							<option value="<?php echo $option; ?>" <?php echo ( isset($_REQUEST['category']) && sanitize_text_field($_REQUEST['category']) && sanitize_text_field($_REQUEST['category']) == $option) ? 'selected="selected" ' : ''; ?>><?php echo $option; ?></option>
						<?php } ?>

					</select>
				</p>

			<?php } ?>

			<p>
				<label for="keywords">Keywords</label>
				<span class="field-instructions">Use commas between words - example: knit, tops, women...</span>
				<input type="text" name="keywords" id="keywords" value="<?php echo ( isset($_REQUEST['keywords']) && sanitize_text_field($_REQUEST['keywords']) ) ? sanitize_text_field($_REQUEST['keywords']) : ''; ?>" placeholder="Type any word" />
			</p>

		</fieldset>


		<?php
		if ( is_array($radio_options) && !empty($radio_options) ) { ?>

			<fieldset class="operation-fields">
				<legend class="company-search-section-title" id="legend-company-type">Company Production Operations</legend>
				<span class="field-instructions">Please check the box of the company production operations you are looking for.</span>

				<ul aria-labelledby="legend-company-type" role="group">
					<?php

					// Show Category Types as checkboxes
					foreach ($radio_options as $key => $term) { ?>
						<li>

							<input type="checkbox" name="operations[]" id="operations-<?php echo $term['name']; ?>" value="<?php echo $term['name']; ?>" <?php echo (isset($_REQUEST['operations']) && in_array($term['name'], $_REQUEST['operations'])) ? 'checked ' : ''; ?>>

							<label class="checkbox-label" for="type-<?php echo $term['name']; ?>"><?php echo str_replace(':','',$term['title'] ); ?></label>
						</li>
					<?php }
					?>
				</ul>
			</fieldset>

		<?php } ?>


		<fieldset class="form-actions">

			<p class="field-instructions">Click the "Search button" and the company profiles regarding your searching criteria will appear. </p>

			<!-- <input type="reset" class="button-large button-secondary" name="submit" id="searchClear" value="Clear form" /> -->
			<input type="submit" class="button-large button-search" name="submit" id="searchSubmit" value="Search"  />
		</fieldset>
	</form>