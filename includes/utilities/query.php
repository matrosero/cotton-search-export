<?php

defined( 'ABSPATH' ) or exit; // or die;

function cotton_args_adminurl( $dl = true, $search_keywords = false, $post__in = array(), $ppp = 40 ) {
    // var_dump($post__in);

    $admin_url = '';

    $args = array(
	
		// Type & Status Parameters
		'post_type'     => 'company',
        'post_status'   => array( 'pending', 'draft', 'publish' ),
        'orderby'       => 'title',
        'order'         => 'ASC',

    );

    


    if ( get_query_var( 'paged' ) && !$dl ) {
        $args['paged'] = get_query_var( 'paged' );
    } elseif ( get_query_var( 'page' ) && !$dl ) {
        $args['paged'] = get_query_var( 'page' );
    }


    
    if ( $dl === true || $search_keywords === true ) {
        $args['posts_per_page'] = -1;
    } else {
        $args['posts_per_page'] = $ppp;
    }

    // var_dump($args);

    //If there was a search, adjust $args
    if ( isset($_REQUEST) ) {


        $meta_query = array();
        
        // Grab submitted
        if ( isset($_REQUEST['keywords']) && sanitize_text_field($_REQUEST['keywords']) ) {
            $keywords = sanitize_text_field($_REQUEST['keywords']);

            
            // $args['s'] = $keywords;

            //If we are searching keywords
            if ( $search_keywords ) {

                // Turn keywords into array
                $keywords = explode(',',str_replace(', ',',',$keywords));
    
                $meta_fields = array(
                    'yarn-sales-product-line',
    
                    // Fabric Sales Product Line: Knit y Woven
                    'knit-fabric',
                    'woven-fabric',
    
                    //Apparel Product Line: Product Description, Product Specialty y Other Product capabilities
                    'product-description',
                    'product-specialty',
                    'other-product-capabilities',
    
                    //Apparel Manufacturing Capabilities: Additional information manufacturing y "Product Development and Design Capabilities"
                    'additional-information-manufacturing',
                    'product-development-and-design-capabilities'
    
                );
    
                foreach($keywords as $value) {
    
                    foreach($meta_fields as $field) {
                        $meta_query[] = array(
                            'key'     => $field,
                            'value'   => $value,
                            'type'    => 'CHAR',
                            'compare' => 'LIKE'
                        );
                    }
                }

                if ( count($meta_query) > 1 ) {
                    $meta_query['relation'] = 'OR';
                }

            } else {

                if ( !empty( $admin_url ) ) 
                $admin_url .= '&';

                $admin_url .= 'keywords='.urlencode($keywords);

            }

        }


        // If not searchign keywords
        if ( !$search_keywords ) {


            if ( !empty($post__in) ) {
                $args['post__in'] = $post__in;
            }


            // if ( isset($_REQUEST['country']) && sanitize_meta( 'factory-country', $_REQUEST['country'], 'post' ) ) {
            
            if ( isset($_REQUEST['country']) && is_array($_REQUEST['country']) ) {

                $countries = $_REQUEST['country'];


                foreach($countries as $key => $country) {
                    $countries[$key] = sanitize_meta( 'factory-country', $country, 'post' );
                }

                
                if ( isset($countries) ) {
                    $meta_query[] = array(
                        'key'     => 'factory-country',//'factory-country',
                        'value'   => $countries,
                        'type'    => 'CHAR',
                        'compare' => 'IN',
                    );
                }

                if ( !empty( $admin_url ) ) 
                    $admin_url .= '&';

                $admin_url .= 'country='.urlencode($country);
            }

            // sanitize_meta( 'factory-country', $_REQUEST['country'], 'post' );
            
            if ( isset($_REQUEST['category']) && sanitize_meta( 'category', $_REQUEST['category'], 'post' ) ) {

                $category = sanitize_meta( 'category', $_REQUEST['category'], 'post' );
                if ( isset($category) ) {
                    $meta_query[] = array(
                        'key'     => 'category',
                        'value'   => $category,
                        'type'    => 'CHAR',
                        'compare' => '=',
                    );
                }

                if ( !empty( $admin_url ) ) 
                    $admin_url .= '&';

                $admin_url .= 'category='.urlencode($category);
            }

            if( isset($_REQUEST['operations']) ) {


                //sanitize every element in array FIX THIS ERROR
                if( array_map( 'esc_attr', $_REQUEST['operations']) ) {
                    $operations = array_map( 'esc_attr', $_REQUEST['operations']);	

                    if ( isset($operations) ) {

                        // print_r( $operations );

                        $operations = cotton_operations_meta_exists($operations);

                        //Ask if it should be if any of THESE, in which case needs changing
                        foreach( $operations as $op) {

                            $meta_query[] = array(
                                'key'     => $op,
                                'value'   => 'Yes',
                                'type'    => 'CHAR',
                                'compare' => '=',
                            );
                        }
                    }

                    foreach ( $operations as $op) {
                        if ( !empty( $admin_url ) ) 
                            $admin_url .= '&';
            
                        $admin_url .= 'operations[]='.$op;
                    } 
                }
            }

            if ( count($meta_query) > 1 ) {
                $meta_query['relation'] = 'AND';
            }
        }


        if ( !empty($args) && !empty($meta_query) ) {
            $args['meta_query'] = $meta_query;
        }
    } 

    if ( !empty( $admin_url ) ) 
			$admin_url .= '&';

	$admin_url .= 'action=print.xsl';

	if ( !empty( $admin_url ) )
		$admin_url = 'admin-post.php?'.$admin_url; 


    $return = array(
        'args' => $args,
        'adminurl' => $admin_url
    );

    return $return;
}