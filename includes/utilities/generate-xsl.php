<?php

defined( 'ABSPATH' ) or exit;

add_action( 'admin_post_print.xsl', 'print_xsl' );
function print_xsl() {

  if ( !wp_verify_nonce( $_REQUEST['_wpnonce'], 'cotton_print_xsl')) {
    php_log('nonce no workie');
    exit("No naughty business please");
  } else {
    php_log('nonce works!');
  }
  
  $temp = cotton_args_adminurl( $dl = true );

  $args = $temp['args'];

  $query = new WP_Query( $args );

    $companies_data = array();

    $posts = $query->posts;

    foreach($posts as $post) {
      $companies_data[] = array(
          'ID' => $post->ID,
          'Company Name' => $post->post_title,
          'Category' => get_post_meta( $post->ID, 'category', 1 ),
          'Representative\'s Name' => get_post_meta( $post->ID, 'representative-name', 1 ),
          'Email' => get_post_meta( $post->ID, 'email', 1 ),
          'Country' => get_post_meta( $post->ID, 'factory-country', 1 ),
          'Corporate Headquarter Address' => get_post_meta( $post->ID,'corporate-headquarter-address', 1 ),
          'Telephone'  => str_replace( '-', ' ', get_post_meta( $post->ID, 'telephone', 1 ) ),
          'Website' => get_post_meta( $post->ID, 'website', 1 ),
      );
    }
    // Filter Customer Data
    function filterCustomerData(&$str) {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }

    // File Name & Content Header For Download
    $file_name = "companies_data.xls";
    // header("Content-Disposition: attachment; filename=\"$file_name\"");
    header("Content-Type: application/vnd.ms-excel");
    header('Content-Disposition: attachment; filename='.$file_name);

    //To define column name in first row.
    $column_names = false;
    // run loop through each row in $companies_data
    foreach($companies_data as $row) {
    if(!$column_names) {
    echo implode("\t", array_keys($row)) . "\n";
    $column_names = true;
    }
    // The array_walk() function runs each array element in a user-defined function.
    array_walk($row, 'filterCustomerData');
    echo implode("\t", array_values($row)) . "\n";
    }
    exit;
}