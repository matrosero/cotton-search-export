<?php

defined( 'ABSPATH' ) or exit; // or die;


//Country
// $clean_value = sanitize_meta( 'factory-country', $input, 'post' );
 
function cotton_sanitize_country_meta( $input ) {

    php_log('checking country: '.$input);

    $input = sanitize_text_field($input);
 
    $options = get_transient( 'cotton_je_countries_options' );

    // // No transients
    if ( empty( $options ) ) {
        php_log('no transient');

        //get all meta, this will create transients
        $meta = cotton_get_je_meta_options();

        //Grab the correct one
        $options = $meta['countries'];
    } else {
        php_log('transient found');
    }

    if ( !in_array( $input, $options) ) {
        // php_log('not an option');
        return false;
    //     wp_die( __( 'Invalid search, go back and try again.', 'textdomain' ) );
    } else {
        // php_log('It is an option');
        return $input;
    }

}
add_filter( 'sanitize_post_meta_factory-country', 'cotton_sanitize_country_meta' );


// Categories
// $clean_value = sanitize_meta( 'category', $input, 'post' );
 
function cotton_sanitize_category_meta( $input ) {

    php_log('checking category: '.$input);

    $input = sanitize_text_field($input);
 
    $options = get_transient( 'cotton_je_categories_options' );

    // // No transients
    if ( empty( $options ) ) {
        php_log('no transient');

        //get all meta, this will create transients
        $meta = cotton_get_je_meta_options();

        //Grab the correct one
        $options = $meta['categories'];
    } else {
        php_log('transient found');
    }

    if ( !in_array( $input, $options) ) {
        // php_log('not an option');
        return false;
    //     wp_die( __( 'Invalid search, go back and try again.', 'textdomain' ) );
    } else {
        // php_log('It is an option');
        return $input;
    }
    
}
add_filter( 'sanitize_post_meta_category', 'cotton_sanitize_category_meta' );


// Check that what is entered in operations exists
function cotton_operations_meta_exists( $input = array() ) {

    if ( empty($input) || !is_array($input) )
        return false;

    // Get meta options    
    $options = get_transient( 'cotton_je_radio_options' );

    // // No transients
    if ( empty( $options ) ) {
        php_log('no transient');

        //get all meta, this will create transients
        $meta = cotton_get_je_meta_options();

        //Grab the correct one
        $options = $meta['options'];

        // sanitize_text_field
    } else {
        php_log('transient found');
    }

    $clean_options = array();

    foreach($options as $option) {
        $clean_options[] = $option['name'];

        // php_log($option['name'].' is an option');
    }

    foreach($input as $key => $value) {
        // php_log('checking that '.$value.' is an option');
        if ( !in_array($value, $clean_options) ) {
            // php_log($value.' is NOT an option');
            unset($input[$key]);
        } else {
            $input[$key] = sanitize_text_field( $value );
        }
    }

    if ( empty($input) ) {
        return false;
    } else {
        return $input;
    }
}


function cotton_get_je_meta_options() {

    // Get transients
    $categories_options = get_transient( 'cotton_je_categories_options' );
    $countries_options = get_transient( 'cotton_je_countries_options' );
    $radio_options = get_transient( 'cotton_je_radio_options' );

    // No transients
    if ( empty( $categories_options ) || empty( $countries_options ) || empty( $radio_options ) ) {

        $categories = array(
            'title' => 'General Information',
            'meta' => 'category',
        );
        
        $countries = array(
            'title' => 'General Information',
            'meta' => 'company-headquarters', //name // title is nice name // options is options
        );
        
        $radio = array(
            'title' => 'Company Production Operations',
            
        );
    
    
        $je_options = get_option( 'jet_engine_meta_boxes' );
    
        $je_meta = array();
    
        foreach ($je_options as $value) {
    
            $name = $value['args']['name'];
            
            switch ($name) {
                case $categories['title']:
        
                    foreach ($value['meta_fields'] as $meta) {
        
                        if ( $meta['name'] == $categories['meta'] ) {

                            $categories_options = array();

                            foreach ($meta['options'] as $option) {

                                if ( $option['value'] ) {
                                    $categories_options[] = $option['value'];
                                }
                            }

                            set_transient( 'cotton_je_categories_options', $categories_options, DAY_IN_SECONDS );
                        }
                    }
                    
        
                case $countries['title']:
        
                    foreach ($value['meta_fields'] as $meta) {
        
                        if ( $meta['name'] == $countries['meta'] ) {
                                
                            $countries_options = array();

                            foreach ($meta['options'] as $option) {

                                if ( $option['value'] ) {
                                    $countries_options[] = $option['value'];
                                }
                            }

                            set_transient( 'cotton_je_countries_options', $countries_options, DAY_IN_SECONDS );
                        }
                    }
                    
                    break;
        
                    
                case $radio['title']:
        
                    $radio_options = $value['meta_fields'];
        
                    foreach ($radio_options as $key => $meta) {
                        
                        // If type is not radio, remove entirely
                        if ( $meta['type'] != 'radio' ) {
                            unset($radio_options[$key]);	
                        } else {
                            $meta['title'] = str_replace(':','',$meta['title'] );
                        }

                        // Remove things that aren't needed
                        unset($radio_options[$key]["width"]);
                        unset($radio_options[$key]["object_type"]);
                        unset($radio_options[$key]["options"]);
                        unset($radio_options[$key]["type"]);
                        unset($radio_options[$key]["isNested"]);
                        unset($radio_options[$key]["description"]);
                        unset($radio_options[$key]["conditional_logic"]);
                        unset($radio_options[$key]["conditions"]);

                    }

                    set_transient( 'cotton_je_radio_options', $radio_options, DAY_IN_SECONDS );
        
                    break;
            }
    
        }
    } else {
        // echo 'transients!';
    }

    $je_meta['categories'] = $categories_options;
    $je_meta['countries'] = $countries_options;
    $je_meta['options'] = $radio_options;


    return $je_meta;
}

function cotton_countries_shortlist() {
    $countries = array(
        'Colombia', 
        'Costa Rica', 
        'Dominican Republic', 
        'Ecuador', 
        'El Salvador', 
        'Guatemala', 
        'Haiti', 
        'Honduras', 
        'Mexico', 
        'Nicaragua', 
        'Peru', 
        'United States',
    );
    return $countries;
}