<?php

defined( 'ABSPATH' ) or exit;

add_action( 'admin_post_print.xsl', 'print_xsl' );
function print_xsl() {

    if ( !wp_verify_nonce( $_REQUEST['_wpnonce'], 'cotton_print_xsl')) {
        php_log('nonce no workie');
        exit("No naughty business please");
    } else {
        php_log('nonce works!');
    }
    

    $post__in = array();

    //If we looked for keywords
    if ( isset($_REQUEST['keywords']) && sanitize_text_field($_REQUEST['keywords']) ) {
        
        // Query for keywords first
        $keywords_args = cotton_args_adminurl( $dl = true, $search_keywords = true );

        $keywords_query = new WP_Query( $keywords_args['args'] );

        if ( $keywords_query->have_posts() ) {

            $posts = $keywords_query->posts;

            foreach($posts as $post) {
                $post__in[] = $post->ID;
            }

        }
    }




    $temp = cotton_args_adminurl( $dl = true, $search_keywords = false, $post__in = $post__in );

    $args = $temp['args'];

    $query = new WP_Query( $args );

    $companies_data = array();

    $posts = $query->posts;

    foreach($posts as $post) {
        $companies_data[] = array(
            // 'ID' => $post->ID,
            'Company Name' => $post->post_title,
            'Category' => get_post_meta( $post->ID, 'category', 1 ),
            'Representative\'s Name' => get_post_meta( $post->ID, 'representative-name', 1 ),
            'Email' => get_post_meta( $post->ID, 'email', 1 ),
            'Country' => get_post_meta( $post->ID, 'factory-country', 1 ),
            'Corporate Headquarter Address' => get_post_meta( $post->ID,'corporate-headquarter-address', 1 ),
            'Telephone'  => str_replace( array('-', '/'), ' ', get_post_meta( $post->ID, 'telephone', 1 ) ),
            'Website' => get_post_meta( $post->ID, 'website', 1 ),
        );
    }
    // Filter Customer Data
    function filterCustomerData(&$str) {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }

    // File Name & Content Header For Download
    $file_name = "companies_data.csv";
    header("Content-Type: application/csv");
    header('Content-Disposition: attachment; filename='.$file_name);
    header('Pragma: no-cache');

    // output the CSV data
    $output = fopen('php://output', 'w');

    // output the column headings
    fputcsv($output, array(
        // 'ID',
        'Company Name',
        'Category',
        'Representative\'s Name',
        'Email',
        'Country',
        'Corporate Headquarter Address',
        'Telephone',
        'Website',
    ));


    foreach($companies_data as $key =>$row) {
        fputcsv($output, $row);
    }
    
    exit;
}