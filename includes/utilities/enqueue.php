<?php

defined( 'ABSPATH' ) or exit; // or die;

function cotton_enqueue_styles() {

    wp_enqueue_style('select2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' );
	wp_enqueue_script('select2', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array('jquery') );
    
    wp_enqueue_style( 'cotton-search', COTTON_PLUGIN_URL . 'assets/styles/style.css', array(), '', 'all' );
    wp_enqueue_style( 'cotton-search-font', 'https://fonts.googleapis.com/css2?family=Raleway:wght@400;600;700&display=swap', array(), '', '' );

    wp_enqueue_script( 'cotton-js', COTTON_PLUGIN_URL . 'assets/scripts/scripts.js', array('jquery'), '', true );
}

add_action( 'wp_enqueue_scripts', 'cotton_enqueue_styles' );

