<?php
/*
 * Basic structure from https://www.smashingmagazine.com/2012/06/front-end-author-listing-user-search-wordpress/
 */

defined( 'ABSPATH' ) or exit; // or die;

// Add the shortcode to WordPress. 
add_shortcode('search-companies', 'cotton_search_shortcode');

function cotton_search_shortcode($atts, $content = null) {

	// Template loader
	$templates = new Cotton_Template_Loader;

    

	ob_start();

	echo '<div class="cotton-search container">';
	
		include( $templates->locate_template( 'search-form.php' ) ); 


		$post__in = array();

		//If we looked for keywords
		if ( isset($_REQUEST['keywords']) && sanitize_text_field($_REQUEST['keywords']) ) {
			
			
			// Query for keywords first
			$keywords_args = cotton_args_adminurl( $dl = false, $search_keywords = true );

			$keywords_query = new WP_Query( $keywords_args['args'] );

			if ( $keywords_query->have_posts() ) {

				$posts = $keywords_query->posts;


				foreach($posts as $post) {
					$post__in[] = $post->ID;
				}
			}
		}


		if ( !isset($_REQUEST['keywords']) || $_REQUEST['keywords'] === '' || ( isset($_REQUEST['keywords']) && !empty($post__in) ) ) {

			$final_args = cotton_args_adminurl( $dl = false, $search_keywords = false, $post__in = $post__in );

			$admin_url = $final_args['adminurl'];
			
			$args = $final_args['args'];
			
			$query = new WP_Query( $args );
			
			// The Loop
			if ( $query->have_posts() ) {
				
				echo '<div class="cotton-search-results">';

				if(isset($_REQUEST)){
					echo '<h2>Search Results</h2>';
					
				}

				$admin_url = wp_nonce_url( admin_url( $admin_url ), 'cotton_print_xsl' );

				echo '<div class="cotton-search-actions">';

				
				echo '<a href="' . get_permalink() . '" class="button button-large  button-search">Show all companies</a>';

				echo '<a href="' . $admin_url . '" class="button button-large button-download">Download results (CSV)</a>';


				echo '</div>';

				echo '<p><strong>Companies Found: '.$query->found_posts.'</strong></p>'; 



				if ( get_query_var( 'paged' ) && get_query_var( 'paged' ) > 1 ) {
					// echo 'option PAGED '.get_query_var( 'paged' );
					$i = 40 *  ( get_query_var( 'paged' ) - 1 );
				} elseif ( get_query_var( 'page' ) && get_query_var( 'page' ) > 1 ) {
					// echo 'option PAGE '.get_query_var( 'page' );
					$i = 40 *  ( get_query_var( 'page' ) - 1 );
				} else {
					// echo 'option other';
					$i = 0;	
				}

				// echo '<br />counting starts at '.$i;

				
				echo '<ul>';
				while ( $query->have_posts() ) {
					$query->the_post();
					$i++;
					?>
					<li id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
					<?php
					echo '<span><strong>'.$i . '.</strong> <a class="cotton-results-title" href="'.get_the_permalink().'">'.get_the_title().'</a></span>';

					// echo '<span>TEL ';
					// echo str_replace('-', ' ',get_post_meta( get_the_ID(), 'telephone', 1 ));
					// echo '</span>';

					echo '<a class="button" href="'.get_the_permalink().'">View Company</a>';
					echo '</li>';
				}
				echo '</ul>';

				?>

				<div class="pagination">
					<?php 

						if ( get_query_var( 'paged' ) ) {
							$paged_in_pagination = get_query_var( 'paged' );
						} elseif ( get_query_var( 'page' ) ) {
							$paged_in_pagination = get_query_var( 'page' );
						} else {
							$paged_in_pagination = 1;
						}

						echo paginate_links( array(
							'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
							'total'        => $query->max_num_pages,
							'current'      => max( 1, $paged_in_pagination ),
							'format'       => '?paged=%#%',
							'show_all'     => false,
							'type'         => 'plain',
							'end_size'     => 2,
							'mid_size'     => 1,
							'prev_next'    => true,
							'prev_text'    => sprintf( '<i></i> %1$s', __( '&lt;', 'text-domain' ) ),
							'next_text'    => sprintf( '%1$s <i></i>', __( '&gt;', 'text-domain' ) ),
							'add_args'     => false,
							'add_fragment' => '',
						) );
					?>
				</div>
				

				<?php

				echo '</div>';

			} else { ?>

				<p>There are no companies with those characteristics.</p>
				
			<?php }
			/* Restore original Post Data */
			wp_reset_postdata();
		} else { ?>

			<p>There are no companies with those characteristics.</p>
			
		<?php }




		
	echo '</div><!-- .company-search -->';

    // Output the content.
    $output = ob_get_contents();
    ob_end_clean();
        
    return $output;
}