jQuery(function($){
	function clearForm() {
		var formCotton = $('#cotton-search');


		formCotton.find('input:text, input:password, input:file, select, textarea').val('');
		formCotton.find('input:radio, input:checkbox')
			 .removeAttr('checked').removeAttr('selected');
	}
	
	var clearBtn = $('#searchClear');

	clearBtn.on( "click", function() {
		clearForm();
	  });


	// to call, use:
	// clear($('#cotton-search')); // by id, recommended
	// resetForm($('form[name=myName]')); // by name
	
});