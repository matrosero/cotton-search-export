<?php
/**
 * Plugin Name:     Cotton Search & Export
 * Plugin URI:      https://gitlab.com/matrosero/cotton-search-export
 * Description:     Search CPT companies based on Jet Engine meta data
 * Author:          Mat Rosero
 * Author URI:      https://matilderosero.com
 * Text Domain:     cotton-search-export
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Cotton_Search_Export
 */

define( 'COTTON_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'COTTON_PLUGIN_URL', plugin_dir_url(__FILE__) ); 

/**
 * Load plugin textdomain.
 *
 * @since 0.1.0
 */
function cotton_load_textdomain() {
	load_plugin_textdomain( 'cotton', false, basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'cotton_load_textdomain' );

/**
 * Templating class.
 *
 * @since 0.1.0
 */
require COTTON_PLUGIN_DIR . '/includes/template-loader/class-gamajo-template-loader.php';
require COTTON_PLUGIN_DIR . '/includes/template-loader/class-cotton-template-loader.php';


/**
 * Shortcodes.
 *
 * @since 0.1.0
 */
require_once( COTTON_PLUGIN_DIR . '/includes/shortcodes/search-shortcode.php' );

/**
 * Utilities.
 *
 * @since 0.1.0
 */
require_once( COTTON_PLUGIN_DIR . '/includes/utilities/enqueue.php' );
require_once( COTTON_PLUGIN_DIR . '/includes/utilities/meta.php' );
require_once( COTTON_PLUGIN_DIR . '/includes/utilities/query.php' );


/**
 * Generate file.
 *
 * @since 0.1.0
 */
require_once( COTTON_PLUGIN_DIR . '/includes/utilities/generate-csv.php' );


if (!function_exists('php_log')) {
    function php_log( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }
}